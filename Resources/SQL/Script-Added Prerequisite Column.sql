USE [master]
GO
/****** Object:  Database [UMSdb]    Script Date: 8/18/2018 9:54:08 PM ******/
CREATE DATABASE [UMSdb]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'UMSdb', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\UMSdb.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'UMSdb_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\UMSdb_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [UMSdb] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [UMSdb].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [UMSdb] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [UMSdb] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [UMSdb] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [UMSdb] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [UMSdb] SET ARITHABORT OFF 
GO
ALTER DATABASE [UMSdb] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [UMSdb] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [UMSdb] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [UMSdb] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [UMSdb] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [UMSdb] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [UMSdb] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [UMSdb] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [UMSdb] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [UMSdb] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [UMSdb] SET  DISABLE_BROKER 
GO
ALTER DATABASE [UMSdb] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [UMSdb] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [UMSdb] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [UMSdb] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [UMSdb] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [UMSdb] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [UMSdb] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [UMSdb] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [UMSdb] SET  MULTI_USER 
GO
ALTER DATABASE [UMSdb] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [UMSdb] SET DB_CHAINING OFF 
GO
ALTER DATABASE [UMSdb] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [UMSdb] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'UMSdb', N'ON'
GO
USE [UMSdb]
GO
/****** Object:  Table [dbo].[Course]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Course](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](50) NOT NULL,
	[Name] [varchar](50) NOT NULL,
	[Credit] [float] NOT NULL,
	[Description] [varchar](500) NULL,
	[Depertment] [int] NOT NULL,
	[Semester] [int] NOT NULL,
	[IsAssigned] [bit] NOT NULL,
	[Prerequisite] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Code] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CourseEnroll]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CourseEnroll](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentId] [int] NOT NULL,
	[CourseId] [int] NOT NULL,
	[Date] [date] NULL,
	[Grade] [varchar](25) NULL,
 CONSTRAINT [PK_CourseEnroll] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CourseTeacher]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CourseTeacher](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CourseId] [int] NOT NULL,
	[TeacherId] [int] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Department]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Department](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [varchar](10) NOT NULL,
	[Name] [varchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Designation]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Designation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RoomAllocation]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RoomAllocation](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CourseId] [int] NOT NULL,
	[RoomId] [int] NOT NULL,
	[Day] [varchar](50) NOT NULL,
	[TimeFrom] [time](7) NOT NULL,
	[TimeTo] [time](7) NOT NULL,
	[IsDeleted] [bit] NULL,
 CONSTRAINT [PK_RoomAlloocation] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Rooms]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Rooms](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoomNo] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Rooms] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Semester]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Semester](
	[Id] [int] NOT NULL,
	[Title] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Semester] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Student]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Student](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RegistrationNo] [varchar](50) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[ContactNo] [varchar](50) NOT NULL,
	[RegistrationDate] [date] NOT NULL,
	[Address] [varchar](500) NOT NULL,
	[Department] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [AK_Table_Column] UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Teacher]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Teacher](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Address] [varchar](500) NOT NULL,
	[Email] [varchar](50) NOT NULL,
	[ContactNo] [varchar](50) NOT NULL,
	[Designation] [int] NOT NULL,
	[Department] [int] NOT NULL,
	[CreditToBeTaken] [float] NOT NULL,
	[CreditTaken] [float] NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
UNIQUE NONCLUSTERED 
(
	[Email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[CourseStaticsView]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[CourseStaticsView]
AS
SELECT        dbo.Department.Id AS DeptId, dbo.Course.Code, dbo.Course.Name, dbo.Semester.Title AS Semester, ISNULL(dbo.Teacher.Name, 'Not Assigned Yet.') AS Teacher, dbo.CourseTeacher.IsDeleted
FROM            dbo.Course INNER JOIN
                         dbo.Semester ON dbo.Course.Semester = dbo.Semester.Id INNER JOIN
                         dbo.Department ON dbo.Course.Depertment = dbo.Department.Id LEFT OUTER JOIN
                         dbo.CourseTeacher ON dbo.Course.Id = dbo.CourseTeacher.CourseId AND dbo.CourseTeacher.IsDeleted <> 'True' LEFT OUTER JOIN
                         dbo.Teacher ON dbo.CourseTeacher.TeacherId = dbo.Teacher.Id


GO
/****** Object:  View [dbo].[ScheduleInfo]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ScheduleInfo]
AS

SELECT        Department.Id AS DeptId, Course.Code, Course.Name, ISNULL(Schedules.Schedule, 'Not Scheduled Yet.') AS Schedules
FROM            Course JOIN
                         Department ON Course.Depertment = Department.Id LEFT OUTER JOIN
                             (SELECT        [CourseId], REPLACE(REPLACE(REPLACE
                                                             ((SELECT        'Room No: ' + CAST
                                                                                              ((SELECT        RoomNo
                                                                                                  FROM            Rooms
                                                                                                  WHERE        Id = RoomId) AS varchar(max)) + ', ' + CAST(Day AS VARCHAR(MAX)) + ', ' + + cast(CONVERT(VARCHAR, TimeFrom, 100) AS varchar(max)) 
                                                                                          + '-' + cast(CONVERT(VARCHAR, TimeTo, 100) AS varchar(max)) AS A
                                                                 FROM            RoomAllocation
                                                                 WHERE        Results.CourseId = CourseId
																 and RoomAllocation.IsDeleted ='False'
																 FOR XML PATH('')), '</A><A>', ';</br>'), '<A>', ''), '</A>', '') AS Schedule
                               FROM            RoomAllocation Results
                               GROUP BY CourseId) Schedules ON Course.Id = Schedules.CourseId



GO
/****** Object:  View [dbo].[ViewResult]    Script Date: 8/18/2018 9:54:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[ViewResult]
AS
SELECT        dbo.Course.Code, dbo.Course.Name, ISNULL(dbo.CourseEnroll.Grade, 'Not Graded Yet') AS Grade, dbo.CourseEnroll.StudentId, dbo.CourseEnroll.CourseId
FROM            dbo.Course LEFT OUTER JOIN
                         dbo.CourseEnroll ON dbo.Course.Id = dbo.CourseEnroll.CourseId


GO
ALTER TABLE [dbo].[Course] ADD  CONSTRAINT [DF_Course_IsAssigned]  DEFAULT ((0)) FOR [IsAssigned]
GO
ALTER TABLE [dbo].[CourseTeacher] ADD  CONSTRAINT [DF_CourseTeacher_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[RoomAllocation] ADD  CONSTRAINT [DF_RoomAllocation_IsDeleted]  DEFAULT ((0)) FOR [IsDeleted]
GO
ALTER TABLE [dbo].[Teacher] ADD  CONSTRAINT [DF_Teacher_CreditTaken]  DEFAULT ((0)) FOR [CreditTaken]
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD FOREIGN KEY([Depertment])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD FOREIGN KEY([Depertment])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD FOREIGN KEY([Prerequisite])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD FOREIGN KEY([Semester])
REFERENCES [dbo].[Semester] ([Id])
GO
ALTER TABLE [dbo].[Course]  WITH CHECK ADD FOREIGN KEY([Semester])
REFERENCES [dbo].[Semester] ([Id])
GO
ALTER TABLE [dbo].[CourseEnroll]  WITH CHECK ADD  CONSTRAINT [FK_CourseEnroll_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[CourseEnroll] CHECK CONSTRAINT [FK_CourseEnroll_Course]
GO
ALTER TABLE [dbo].[CourseEnroll]  WITH CHECK ADD  CONSTRAINT [FK_CourseEnroll_Student] FOREIGN KEY([StudentId])
REFERENCES [dbo].[Student] ([Id])
GO
ALTER TABLE [dbo].[CourseEnroll] CHECK CONSTRAINT [FK_CourseEnroll_Student]
GO
ALTER TABLE [dbo].[CourseTeacher]  WITH CHECK ADD  CONSTRAINT [CourseIdFK] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[CourseTeacher] CHECK CONSTRAINT [CourseIdFK]
GO
ALTER TABLE [dbo].[CourseTeacher]  WITH CHECK ADD  CONSTRAINT [TeacherIdFK] FOREIGN KEY([TeacherId])
REFERENCES [dbo].[Teacher] ([Id])
GO
ALTER TABLE [dbo].[CourseTeacher] CHECK CONSTRAINT [TeacherIdFK]
GO
ALTER TABLE [dbo].[RoomAllocation]  WITH CHECK ADD  CONSTRAINT [FK_RoomAlloocation_Course] FOREIGN KEY([CourseId])
REFERENCES [dbo].[Course] ([Id])
GO
ALTER TABLE [dbo].[RoomAllocation] CHECK CONSTRAINT [FK_RoomAlloocation_Course]
GO
ALTER TABLE [dbo].[RoomAllocation]  WITH CHECK ADD  CONSTRAINT [FK_RoomAlloocation_Rooms] FOREIGN KEY([RoomId])
REFERENCES [dbo].[Rooms] ([Id])
GO
ALTER TABLE [dbo].[RoomAllocation] CHECK CONSTRAINT [FK_RoomAlloocation_Rooms]
GO
ALTER TABLE [dbo].[Student]  WITH CHECK ADD  CONSTRAINT [FK_studentDept_ToTable] FOREIGN KEY([Department])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[Student] CHECK CONSTRAINT [FK_studentDept_ToTable]
GO
ALTER TABLE [dbo].[Teacher]  WITH CHECK ADD  CONSTRAINT [FK_Teacher_Department] FOREIGN KEY([Department])
REFERENCES [dbo].[Department] ([Id])
GO
ALTER TABLE [dbo].[Teacher] CHECK CONSTRAINT [FK_Teacher_Department]
GO
ALTER TABLE [dbo].[Teacher]  WITH CHECK ADD  CONSTRAINT [FK_Teacher_Designation] FOREIGN KEY([Designation])
REFERENCES [dbo].[Designation] ([Id])
GO
ALTER TABLE [dbo].[Teacher] CHECK CONSTRAINT [FK_Teacher_Designation]
GO
USE [master]
GO
ALTER DATABASE [UMSdb] SET  READ_WRITE 
GO
