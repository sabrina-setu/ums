USE [UMSdb]
GO

INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'2', N'Student')
GO
INSERT [dbo].[AspNetRoles] ([Id], [Name]) VALUES (N'1', N'Teacher')
GO

INSERT [dbo].[AspNetUsers] ([Id], [Email], [EmailConfirmed], [PasswordHash], [SecurityStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEndDateUtc], [LockoutEnabled], [AccessFailedCount], [UserName]) VALUES (N'803f044d-53d0-4a6c-9840-f06c37a25062', N'teacher@admin.user', 0, N'AJmfUBGQdCtHd3DbM6LRiXugKXxedT6DO19M2gAfoIbG0c7jD25Zve+NkfMU2pkLZw==', N'8b438eda-134f-4ab1-8fc1-40070a8047dc', NULL, 0, 0, NULL, 0, 0, N'teacher@admin.user')
GO
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'803f044d-53d0-4a6c-9840-f06c37a25062', N'1')
GO

SET IDENTITY_INSERT [dbo].[Teacher] ON 
GO
INSERT [dbo].[Teacher] ([Id], [Name], [Address], [Email], [ContactNo], [Designation], [Department], [CreditToBeTaken], [CreditTaken], [UserId]) VALUES (4, N'Teacher/Admin', N'Sample Address', N'teacher@admin.user', N'1234567890', 1, 1, 0, 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[Teacher] OFF
GO
