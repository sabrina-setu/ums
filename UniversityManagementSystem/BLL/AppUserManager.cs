﻿using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class AppUserManager
    {
        public bool CreateUser(Student student)
        {
            var user = new ApplicationUser() { Email = student.Email, UserName = student.Email };
            var res = new AppUserGateway().CreateUser(user, "Student");
            student.UserId = user.Id;
            return res;
        }

        public bool CreateUser(Teacher teacher)
        {
            var user = new ApplicationUser() { Email = teacher.Email, UserName = teacher.Email };
            var res = new AppUserGateway().CreateUser(user, "Teacher");
            teacher.UserId = user.Id;
            return res;
        }
    }
}