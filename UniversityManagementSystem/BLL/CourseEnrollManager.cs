﻿using System;
using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class CourseEnrollManager
    {
        private CourseEnrollGateway courseEnrollGateway = new CourseEnrollGateway();

        public string EnrollCourse(CourseEnrollViewModel courseEnrollView)
        {
            try
            {
                if (courseEnrollGateway.EnrollCourse(courseEnrollView)) return "Enrolled Successfully.";
                return "Failed";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public string SaveResult(int studentId, int courseId, string grade)
        {
            try
            {
                if (courseEnrollGateway.SaveResult(studentId, courseId, grade))
                    return "Saved Successfully.";
                return "Failed";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Student> GetEnrolledStudents()
        {
            return courseEnrollGateway.GetEnrolledStudents();
        }

        public List<Course> GetEnrolledCourseByStudentId(int studentId)
        {
            return courseEnrollGateway.GetEnrolledCourseByStudentId(studentId);
        }

        public List<CourseEnrollViewModel> GetResultById(int studentId)
        {
            return courseEnrollGateway.GetResultById(studentId);
        }

        public Course GetIncompletePrerequisite(int studentId, int courseId)
        {
            return courseEnrollGateway.GetIncompletePrerequisite(studentId, courseId);
        }
    }
}