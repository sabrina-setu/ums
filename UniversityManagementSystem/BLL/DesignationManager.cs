﻿using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class DesignationManager
    {
        private DesignationGateway designationGateway = new DesignationGateway();

        public List<Designation> GetAllDesignation()
        {
            return designationGateway.GetAllDesignations();
        } 
    }
}