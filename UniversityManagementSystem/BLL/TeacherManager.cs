﻿using System;
using System.Collections.Generic;
using UniversityManagementSystem.DAL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.BLL
{
    public class TeacherManager
    {
        TeacherGateway teacherGateway=new TeacherGateway();
        public string SaveTeacher(Teacher teacher)
        {
            try
            {
                if (teacherGateway.IsDuplicate(teacher)) return "Email already entered.";
                if (teacherGateway.SaveTeacher(teacher)) return "Saved successfully.";
                return "Save failed";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<Teacher> GetTeacherByDept(int deptid)
        {
            return teacherGateway.GetTeacherByDept(deptid);
        }

        public Teacher GetTeacherById(int id)
        {
            return teacherGateway.GetTeacherById(id);
        }
    }
}