﻿using System.Web.Mvc;
using UniversityManagementSystem.BLL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Controllers
{
    public class CourseTeacherController : Controller
    {
        private CourseTeacherManager courseTeacherManager = new CourseTeacherManager();
        private DepartmentManager departmentManager = new DepartmentManager();
        private TeacherManager teacherManager = new TeacherManager();
        private CourseManager courseManager = new CourseManager();

        // GET: CourseTeacher
        [Authorize(Roles = "Teacher")]
        public ActionResult Index()
        {
            ViewBag.Title = "Course Assign";
            ViewBag.Departments = departmentManager.GetallDepartments();
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult Index(int courseId, int teacherId)
        {
            ViewBag.Msg = courseTeacherManager.AssignCourse(courseId, teacherId);
            ViewBag.Title = "Course Assign";
            ViewBag.Departments = departmentManager.GetallDepartments();
            return View();
        }

        public ActionResult ViewStatic()
        {
            ViewBag.Title = "Course Static";
            ViewBag.Departments = departmentManager.GetallDepartments();
            return View();
        }

        [Authorize(Roles = "Teacher")]
        public ActionResult UnassignCourses()
        {
            ViewBag.Title = "Unassign Courses";
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult UnassignCourses(bool id)
        {
            if(id) ViewBag.Msg = courseTeacherManager.UnassignAllCourses();
            ViewBag.Title = "Unassign Courses";
            return View();
        }

        public JsonResult GetStaticByDept(int departmentId)
        {
            var statics = courseTeacherManager.GetCourseStaticByDept(departmentId);
            return Json(statics, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTeacherByDept(int deptId)
        {
            var teachers = teacherManager.GetTeacherByDept(deptId);
            return Json(teachers, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCourseByDept(int deptId)
        {
            var courses = courseManager.GetUnassignedCourse(deptId);
            return Json(courses, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetCourseById(int courseId)
        {
            var course = courseManager.GetCourseById(courseId);
            return Json(course, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetTeacherById(int teacherId)
        {
            var teacher = teacherManager.GetTeacherById(teacherId);
            return Json(teacher, JsonRequestBehavior.AllowGet);
        }
    }
}