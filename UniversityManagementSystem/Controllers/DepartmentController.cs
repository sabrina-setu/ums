﻿using System.Web.Mvc;
using UniversityManagementSystem.BLL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Controllers
{
    public class DepartmentController : Controller
    {
        private DepartmentManager departmentManager = new DepartmentManager();

        // GET: Department
        [Authorize(Roles = "Teacher")]
        public ActionResult Index()
        {
            ViewBag.Title = "Save Department";
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult Index(Department department)
        {
            ViewBag.Title = "Save Department";
            ViewBag.Msg = departmentManager.SaveDepartment(department);
            ModelState.Clear();
            return View();
        }

        public ActionResult ViewDepartments()
        {
            ViewBag.Title = "View Departments";
            return View(departmentManager.GetallDepartments());
        }
    }
}