﻿using System.Web.Mvc;
using UniversityManagementSystem.BLL;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.Controllers
{
    public class TeacherController : Controller
    {
        private TeacherManager teacherManager = new TeacherManager();
        private DepartmentManager departmentManager = new DepartmentManager();
        private DesignationManager designationManager = new DesignationManager();
        private AppUserManager appUserManager = new AppUserManager();

        // GET: Teacher
        [Authorize(Roles = "Teacher")]
        public ActionResult SaveTeacher()
        {
            ViewBag.Designations = designationManager.GetAllDesignation();
            ViewBag.Departments = departmentManager.GetallDepartments();
            ViewBag.Title = "Save Teacher";
            return View();
        }

        [HttpPost]
        [Authorize(Roles = "Teacher")]
        public ActionResult SaveTeacher(Teacher teacher)
        {
            appUserManager.CreateUser(teacher);
            ViewBag.Msg = teacherManager.SaveTeacher(teacher);
            ViewBag.Designations = designationManager.GetAllDesignation();
            ViewBag.Departments = departmentManager.GetallDepartments();
            ViewBag.Title = "Save Teacher";
            ModelState.Clear();
            return View();
        }
    }
}