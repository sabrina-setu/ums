﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class AppUserGateway
    {
        public bool CreateUser(ApplicationUser user, string role)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));
            var result = UserManager.Create(user, "123456");
            UserManager.AddToRole(user.Id, role);

            return result.Succeeded ? true : false;
        }
    }
}