﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class DesignationGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon = new SqlConnection(conString);

        public List<Designation> GetAllDesignations()
        {
            List<Designation> designations = new List<Designation>();

            string query = "select * from Designation;";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    designations.Add(new Designation()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Title = getReader["Title"].ToString()
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return designations;
        }
    }
}