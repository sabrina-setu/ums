﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class ScheduleGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon = new SqlConnection(conString);
        

        public bool AllocateRoom(Schedule schedule)
        {
            string query = @"insert into RoomAllocation(CourseId, RoomId, Day, TimeFrom, TimeTo)" +
                           " values(" + schedule.CourseId + "," + schedule.RoomId + "," +
                           "'" + schedule.Day + "','" + schedule.FromTime.ToString("HH:mm") + "'," +
                           "'" + schedule.ToTime.ToString("HH:mm") + "');";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query, _universityCon);
            int rowCount = saveCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public bool Overlap(Schedule schedule)
        {
            string query =
                "select * from RoomAllocation where RoomId = " + schedule.RoomId+
                "and Day = '"+schedule.Day+"' " +
                "and((TimeFrom = '" + schedule.FromTime.ToString("HH:mm") + "' and TimeTo = '" + schedule.ToTime.ToString("HH:mm") + "') " +
                "or (TimeFrom > '" + schedule.FromTime.ToString("HH:mm") + "' and TimeTo < '" + schedule.ToTime.ToString("HH:mm") + "') " +
                "or (TimeFrom <= '" + schedule.ToTime.ToString("HH:mm") + "' and TimeTo >= '" + schedule.ToTime.ToString("HH:mm") + "') " +
                "or(TimeFrom <= '" + schedule.FromTime.ToString("HH:mm") + "' and TimeTo >= '" + schedule.FromTime.ToString("HH:mm") + "') " +
                "or (TimeFrom < '" + schedule.FromTime.ToString("HH:mm") + "' and TimeTo > '" + schedule.ToTime.ToString("HH:mm") + "')) " +
                "AND IsDeleted!= 'True';";

            _universityCon.Open();
            SqlCommand checkCommand = new SqlCommand(query, _universityCon);
            SqlDataReader duplicateReader = checkCommand.ExecuteReader();
            bool duplicate = duplicateReader.HasRows;
            duplicateReader.Close();
            _universityCon.Close();
            return duplicate;
        }
        public List<Schedule> GetScheduleByDept(int deptId)
        {
            List<Schedule> schedules = new List<Schedule>();

            string query = "select * from ScheduleInfo where DeptId="+deptId+";";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    schedules.Add(new Schedule()
                    {
                        CourseCode = getReader["Code"].ToString(),
                        CourseName = getReader["Name"].ToString(),
                        ScheduleInfo = getReader["Schedules"].ToString(),
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return schedules;
        }

        public List<Schedule> GetAllRooms()
        {
            List<Schedule> schedules = new List<Schedule>();

            string query = "select * from Rooms";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    schedules.Add(new Schedule()
                    {
                        RoomId = Convert.ToInt32(getReader["Id"]),
                        RoomNo = getReader["RoomNo"].ToString(),
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return schedules;
        }
        public bool UnallocateAllRooms()
        {
            string query = "update RoomAllocation set IsDeleted='True';";
            _universityCon.Open();
            SqlCommand unassignCommand = new SqlCommand(query, _universityCon);
            int rowCount = unassignCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0)
                return true;
            return false;
        }

    }
}