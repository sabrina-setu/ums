﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class StudentGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon = new SqlConnection(conString);

        public bool SaveStudent(Student student)
        {
            string query =
                @"INSERT INTO Student(RegistrationNo,Name,Email,ContactNo,RegistrationDate,Address,Department, Intake, Section) VALUES('" +
                student.RegistrationNo + "','" + student.Name + "','" + student.Email + "','" + student.ContactNo + "','" +
                student.RegistrationDate.ToShortDateString() + "','" + student.Address + "'," + student.DepartmentName.Id + ", "+student.Intake+", "+student.Section+");";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query, _universityCon);
            int rowCount = saveCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public bool IsDuplicate(Student student)
        {
            string query = "select * from Student where Email='" + student.Email + "';";
            _universityCon.Open();
            SqlCommand checkCommand = new SqlCommand(query, _universityCon);
            SqlDataReader duplicateReader = checkCommand.ExecuteReader();
            bool duplicate = duplicateReader.HasRows;
            duplicateReader.Close();
            _universityCon.Close();
            return duplicate;
        }
        
        public List<Student> GetAllstStudents()
        {
            List<Student> students  = new List<Student>();

            string query = "select * from Student;";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    students.Add(new Student()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        RegistrationNo = getReader["RegistrationNo"].ToString(),
                        Name = getReader["Name"].ToString(),
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return students;
        }
        public int GetNumofStudent(int deptId)
        {
            string query =
                @"select count(*) from Student where Department=" + deptId + " and year(RegistrationDate)=" +
                DateTime.Now.Year + ";";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query, _universityCon);
            int rowCount = (int) saveCommand.ExecuteScalar();
            _universityCon.Close();
            return rowCount;
        }

        public Student GetStStudentById(int id)
        {
            Student student = new Student();
            string query = @"select Student.Id,Student.RegistrationNo, Student.Name,Email,Department.Id as DId, Department.Name as DName
                                from Student, Department
                                where Student.Department=Department.Id
                                and Student.Id="+id+";";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    student.Id = Convert.ToInt32(getReader["Id"]);
                    student.RegistrationNo = getReader["RegistrationNo"].ToString();
                    student.Name = getReader["Name"].ToString();
                    student.Email = getReader["Email"].ToString();
                    student.DepartmentName = new Department()
                    {
                        Id = Convert.ToInt32(getReader["DId"].ToString()),
                        Name = getReader["DName"].ToString()
                    };
                }
            }
            getReader.Close();
            _universityCon.Close();
            return student;
        }
    }
}