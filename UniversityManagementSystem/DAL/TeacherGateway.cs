﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Web.Configuration;
using UniversityManagementSystem.Models;

namespace UniversityManagementSystem.DAL
{
    public class TeacherGateway
    {
        private static string conString = WebConfigurationManager.ConnectionStrings["UniversityCon"].ConnectionString;
        SqlConnection _universityCon = new SqlConnection(conString);

        public bool SaveTeacher(Teacher teacher)
        {
            string query =
                @"INSERT INTO [dbo].[Teacher] ([Name], [Address], [Email], [ContactNo], [Designation], [Department], [CreditToBeTaken])
                VALUES ('" + teacher.Name + "','" + teacher.Address + "','" + teacher.Email + "','" + teacher.ContactNo +
                "'," + teacher.DesignationName.Id + "," + teacher.DepartmentName.Id + "," + teacher.CreditToBeTaken +
                ")";
            _universityCon.Open();
            SqlCommand saveCommand = new SqlCommand(query, _universityCon);
            int rowCount = saveCommand.ExecuteNonQuery();
            _universityCon.Close();
            if (rowCount > 0) return true;
            return false;
        }

        public bool IsDuplicate(Teacher teacher)
        {
            string query = "select * from Teacher where Email='"+teacher.Email+"'";
            _universityCon.Open();
            SqlCommand checkCommand = new SqlCommand(query, _universityCon);
            SqlDataReader duplicateReader = checkCommand.ExecuteReader();
            bool duplicate = duplicateReader.HasRows;
            duplicateReader.Close();
            _universityCon.Close();
            return duplicate;
        }
        //not complete
        public List<Teacher> GetAllTeacher()
        {
            List<Teacher> teachers = new List<Teacher>();

            string query = "select * from Teacher;";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    teachers.Add(new Teacher()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Name = getReader["Name"].ToString()
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return teachers;
        }

        public List<Teacher> GetTeacherByDept(int deptid)
        {
            List<Teacher> teachers = new List<Teacher>();

            string query = "select * from Teacher where Department=" + deptid + ";";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    teachers.Add(new Teacher()
                    {
                        Id = Convert.ToInt32(getReader["Id"]),
                        Name = getReader["Name"].ToString(),
                        CreditToBeTaken = Convert.ToDouble(getReader["CreditToBeTaken"]),
                        CreditTaken = Convert.ToDouble(getReader["CreditTaken"])
                    });
                }
            }
            getReader.Close();
            _universityCon.Close();
            return teachers;
        }

        public Teacher GetTeacherById(int id)
        {
            Teacher teacher=new Teacher();
            string query = "select * from Teacher where Id=" + id + ";";
            _universityCon.Open();
            SqlCommand getCommand = new SqlCommand(query, _universityCon);
            SqlDataReader getReader = getCommand.ExecuteReader();
            if (getReader.HasRows)
            {
                while (getReader.Read())
                {
                    teacher.Id = Convert.ToInt32(getReader["Id"]);
                    teacher.Name = getReader["Name"].ToString();
                    teacher.CreditToBeTaken = Convert.ToDouble(getReader["CreditToBeTaken"]);
                    teacher.CreditTaken = Convert.ToDouble(getReader["CreditTaken"]);
                }
            }
            getReader.Close();
            _universityCon.Close();
            return teacher;
        }

    }
}