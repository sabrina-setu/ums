﻿using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Models
{
    public class Course
    {
        public int Id { get; set; }
        [Required]
        [MinLength(5, ErrorMessage = "Code must be at least 5 character.")]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [Range(0.5, 5, ErrorMessage = "Credit must be 0.5 to 5.0.")]
        public double Credit { get; set; }
        public string Description { get; set; }

        [Required]
        [Range(1.0, double.PositiveInfinity, ErrorMessage = "Please Select department.")]
        public int DepartmentId { get; set; }

        public string DepartmentName { get; set; }
        [Required]
        [Range(1, int.MaxValue)]
        public int SemesterId { get; set; }

        public string SemesterTitle { get; set; }

        public int PrerequisiteId { get; set; }
        public string PrerequisiteCourseName { get; set; }
    }
}