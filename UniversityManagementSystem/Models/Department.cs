﻿using System.ComponentModel.DataAnnotations;
using System.Web.Services.Description;

namespace UniversityManagementSystem.Models
{
    public class Department
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(7, ErrorMessage = "Code length should be 2 to 7 character."),
            MinLength(2, ErrorMessage = "Code length should be 2 to 7 character.")]
        public string Code { get; set; }
        [Required]
        public string Name { get; set; }
    }
}