﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Models
{
    public class Student
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        [Required]
        [DisplayName("Registration No.")]
        public string RegistrationNo { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        [RegularExpression(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$",
            ErrorMessage = "Email must be in correct format.")]
        public string Email { get; set; }
        [Required]
        [DisplayName("Contact No.")]
        public string ContactNo { get; set; }
        [Required]
        [DisplayName("Registration Date")]
        [DataType(DataType.Date)]
        public DateTime RegistrationDate { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [DisplayName("Department")]
        public Department DepartmentName { get; set; }
        [Required]
        public int Intake { get; set; }
        [Required]
        public int Section { get; set; }
    }
}