﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace UniversityManagementSystem.Models
{
    public class Teacher
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        [RegularExpression(@"^[\w!#$%&'*+\-/=?\^_`{|}~]+(\.[\w!#$%&'*+\-/=?\^_`{|}~]+)*@((([\-\w]+\.)+[a-zA-Z]{2,4})|(([0-9]{1,3}\.){3}[0-9]{1,3}))$",
            ErrorMessage = "Email must be in correct format.")]
        public string Email { get; set; }   
        [Required]
        [DisplayName("Contact No.")]
        public string ContactNo { get; set; }
        public Designation DesignationName { get; set; }
        public Department DepartmentName { get; set; }
        [Required]
        [Range(0,1000,ErrorMessage = "Must be non negetive.")]
        public double CreditToBeTaken { get; set; }
        public double CreditTaken { get; set; }
    }
}